// import 'vuetify/dist/vuetify.min.css';
// import 'material-design-icons-iconfont/dist/material-design-icons.css';

import 'font-awesome/css/font-awesome.min.css';

import Vue from 'vue';
import Vuetify from 'vuetify';
import axios from 'axios';
import join from 'url-join';
import lockr from 'lockr';
import VueClipboard from 'vue-clipboard2';

import App from './App.vue';
import router from './router';
import store from './store';

import AuthService from './services/Auth';

import './registerServiceWorker';

Vue.use(Vuetify);
Vue.use(VueClipboard);

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Content-Type'] = 'application/json';

const isAbsoluteURLRegex = /^(?:\w+:)\/\//
const authService = new AuthService();

/**
 * Set base url
 */
axios.interceptors.request.use((config) => {
  // Concatenate base path if not an absolute URL
  if (!isAbsoluteURLRegex.test(config.url)) {
    config.url = join(process.env.VUE_APP_BASE_URI, config.url);
  }

  return config;
});

/**
 * Set Authorization token to Request
 */
axios.interceptors.request.use((config) => {
  if (authService.isLoggedIn()) {
    config.headers.common['Authorization'] = 'Bearer ' + authService.getToken();
  }

  return config;
});

/**
 *
 */
router.afterEach(function (to, from, next) {
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

/**
 * External Auth hook
 */
router.beforeEach((to, from, next) => {
  if (to.query.hasOwnProperty('access_token')) {
    authService.setToken(to.query.access_token);

    axios.post('/auth/me')
      .then((response) => {
        authService.setUser(response.data);

        delete to.query.access_token;;
        window.location = to.path;
      })
      .catch(() => {
        lockr.rm('token');
        lockr.rm('user');

        window.location = process.env.VUE_APP_LANDING_URI
      });
  } else {
    next();
  }
});

/**
 * Roles page checker
 */
router.beforeEach((to, from, next) => {
  if (to.meta.hasOwnProperty('roles') && authService.isLoggedIn()) {
    if (!to.meta.roles.includes(authService.getRoleUserRole().name)) {
      window.location.href = process.env.VUE_APP_LANDING_URI;
    } else {
      next();
    }
  } else {
    next();
  }
});

/**
 * Close menu on mobile devices
 */
router.afterEach((to, from) => {
  const leftDrawerEL = document.querySelector('#left-drawer');

  const body = document.querySelector('body');
  const mq = window.matchMedia('(min-width: 960px)');

  if (!mq.matches) {
    leftDrawerEL.classList.remove('mdc-drawer--persistent', 'mdc-drawer--open');
    leftDrawerEL.classList.add('mdc-drawer--temporary', 'mdc-drawer--closed');
  }

  body.classList.remove('mdc-drawer-scroll-lock');
});

/**
 * Guarded page checker
 */
router.beforeEach((to, from, next) => {
  if (to.meta.hasOwnProperty('guarded') && !authService.isLoggedIn()) {
    window.location.href = process.env.VUE_APP_LANDING_URI;
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  let required = [ 'phone', 'telegram_name'];


  if (!authService.isLoggedIn()) {
    next();
  }


  try {
    axios.post('/auth/me')
      .then((response) => {
        if (response === undefined) {
          next();
        }

        const user = response.data
        authService.setUser(user);

        let requiredStatus = true

        if (authService.getRoleUserRole().name === 'ROLE_ADMIN') {
            // required = [ 'phone', 'telegram_name' ] // add 'host', 'channel_premium'
        }

        required.forEach((item) => {
          if (!user[item]) {
            requiredStatus = false;
          }
        });


        if (
          to.name !== 'Profile'
          && authService.isLoggedIn()
          // && (user.hasOwnProperty('roles') && user.roles[0].name !== 'ROLE_ADMIN')
          && !requiredStatus
        ) {
          // app.$toastr('warning', 'Please, provide full information')

          store.dispatch('switchProfileRequired', true);

          next({
            path: '/profile',
            meta: {
              flash: true,
            },
          });
        } else {
          next();
        }
      });
  } catch (e) {}
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
