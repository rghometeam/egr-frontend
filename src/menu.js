
export default [
  {
    title: 'Dashboard',
    href: '/dashboard',
    icon: 'dashboard',
    roles: [
      'ROLE_USER', 'ROLE_FREE', 'ROLE_PREMIUM', 'ROLE_EDITOR', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'
    ]//, badge: 4,
  },
  // {
  //   title: 'Builder',
  //   href: '/builder',
  //   icon: 'dashboard',
  //   roles: [
  //     'ROLE_ADMIN'
  //   ]
  //   //, badge: 4,
  // },

  {
    title: 'Payments',
    href: '/transactions',
    icon: 'compare_arrows',
    roles: [
      'ROLE_USER', 'ROLE_FREE', 'ROLE_PREMIUM', 'ROLE_ADMIN', 'ROLE_EDITOR', 'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Owners',
    href: '/owners',
    icon: 'person',
    roles: [
        'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Users',
    href: '/users',
    icon: 'person',
    roles: [
      'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Packages',
    href: '/packages',
    icon: 'view_module',
    roles: [
      'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Logs',
    href: '/logs',
    icon: 'line_style',
    roles: [
      'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Addresses',
    href: '/addresses',
    icon: 'low_priority',
    roles: [
      'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Black List',
    href: '/black-list',
    icon: 'list',
    roles: [
        'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Settings',
    href: '/settings',
    icon: 'settings',
    roles: [
      'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Email Details',
    href: '/email-details',
    icon: 'person',
    roles: [
      'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Portfolio',
    href: '/portfolio',
    icon: 'chrome_reader_mode',
    roles: [
    ]
  },
  {
    title: 'Referrals',
    href: '/referrals',
    icon: 'format_indent_increase',
    roles: [
    ]
  },
  {
    title: 'Active Signals',
    href: '/signals/active',
    icon: 'person',
    roles: [
       'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Ended Signals',
    href: '/signals/ended',
    icon: 'person',
    roles: [
       'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Canceled Signals',
    href: '/signals/canceled',
    icon: 'person',
    roles: [
      'ROLE_SUPER_ADMIN'
    ]
  },
  {
    title: 'Profile',
    href: '/profile',
    icon: 'person',
    roles: [
      'ROLE_USER', 'ROLE_FREE', 'ROLE_PREMIUM', 'ROLE_EDITOR', 'ROLE_SUPER_ADMIN', 'ROLE_ADMIN'
    ]
  },
];
