import Vue from 'vue';
import Router from 'vue-router';
// import About from './views/About.vue';

// import LandingLogin from './views/Landing/Login.vue';
// import LandingRegister from './views/Landing/Register.vue';
// import LandingHome from './views/Landing/Home.vue';

import Dashboard from './views/Dashboard.vue';
import Payment from './views/Payment.vue';
import Profile from './views/Profile.vue';
import Transactions from './views/Transactions.vue';
import Referrals from './views/Referrals.vue';
import Builder from './views/Builder.vue';
import Users from './views/Users.vue';
import Packages from './views/Packages.vue';
import Logs from './views/Logs.vue';
import Addresses from './views/Addresses.vue';
import BlackList from './views/BlackList.vue';
import Settings from './views/Settings.vue';
import EmailDetails from './views/EmailDetails.vue';
import Portfolio from './views/Portfolio.vue';
import ActiveSignals from './views/ActiveSignals.vue';
import CanceledSignals from './views/CanceledSignals.vue';
import EndedSignals from './views/EndedSignals.vue';
import Tools from './views/Tools.vue';
import Owners from './views/Owners.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "*",
      redirect: Dashboard // to => { window.location = '/dashboard' }
    },
    // {
    //   path: '/',
    //   name: 'LandingHome',
    //   component: LandingHome,
    // },
    // {
    //   path: '/login',
    //   name: 'LandingLogin',
    //   component: LandingLogin,
    // },
    // {
    //   path: '/sign-up',
    //   name: 'LandingRegister',
    //   component: LandingRegister,
    // },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/owners',
      name: 'Owners',
      component: Owners,
      meta: {
          guarded: true,
      },
    },
    {
      path: '/tools',
      name: 'Tolls',
      component: Tools,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/signals/active',
      name: 'ActiveSignals',
      component: ActiveSignals,
      meta: {
          guarded: true,
      },
    },
    {
      path: '/signals/canceled',
      name: 'CanceledSignals',
      component: CanceledSignals,
      meta: {
          guarded: true,
      },
    },
    {
      path: '/signals/ended',
      name: 'EndedSignals',
      component: EndedSignals,
      meta: {
          guarded: true,
      },
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/builder',
      name: 'Builder',
      component: Builder,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/logs',
      name: 'Logs',
      component: Logs,
      // meta: {
      //   guarded: true,
      // },
    },
    {
      path: '/email-details',
      name: 'EmailDetails',
      component: EmailDetails,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/black-list',
      name: 'BlackList',
      component: BlackList,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/addresses',
      name: 'Addresses',
      component: Addresses,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/transactions',
      name: 'Payments',
      component: Transactions,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/packages',
      name: 'Packages',
      component: Packages,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/referrals',
      name: 'Referrals',
      component: Referrals,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/payment',
      name: 'Payment',
      component: Payment,
      meta: {
        guarded: true,
      },
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {
        //guarded: true,
      },
    },
    {
      path: '/portfolio',
      name: 'Portfolio',
      component: Portfolio,
      meta: {
        //guarded: true,
      },
    },
  ],
});
