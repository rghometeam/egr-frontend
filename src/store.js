import Vue from 'vue';
import Vuex from 'vuex';
import lockr from 'lockr';

Vue.use(Vuex);

function currentLayout() {
  // const needLanding = [
  //   '/',
  //   '/sign-up',
  //   '/login',
  // ];
  //
  // if (needLanding.includes(window.location.pathname)) {
  //   return 'landing';
  // }

  const storageLayout = lockr.get('layout');

  if (storageLayout) {
    return storageLayout;
  }

  return 'classic';
}

export default new Vuex.Store({
  state: {
    // layout: 'classic',
    layout: currentLayout(),
    profileRequired: false,
  },
  mutations: {
    SET_LAYOUT(state, payload) {
      // eslint-disable-next-line no-param-reassign
      state.layout = payload;
    },
    SET_PROFILE_REQUIRED(state, payload) {
      state.profileRequired = payload;
    }
  },
  getters: {
    layout(state) {
      return state.layout;
    },
    profileRequired(state) {
      return state.profileRequired;
    }
  },
  actions: {
    switchLayout({commit}, layout) {
      commit('SET_LAYOUT', layout)
    },
    switchProfileRequired({commit}, flag) {
      commit('SET_PROFILE_REQUIRED', flag)
    }
  }
});
