const path = require('path')

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    devtool: 'source-map',
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    },
    output: {
      filename: (process.env.NODE_ENV === 'production') ? '[name].[chunkhash].bundle.js' : '[name].[hash].bundle.js',
      chunkFilename: (process.env.NODE_ENV === 'production') ? '[name].[chunkhash].bundle.js' : '[name].[hash].bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    module: {
      rules: [
        // {
        //   test: require.resolve('wowjs/dist/wow.min.js'),
        //   loader: 'exports?this.WOW',
        // },
        {
          test: /\.styl$/,
          loader: ['style-loader', 'css-loader', 'stylus-loader']
        },
        {
          test: /\.(sass|scss)$/,
          use: [
            // 'style-loader',
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                includePaths: [path.resolve(__dirname, 'node_modules')],
              },
            },
          ],
        },
      ],
    },
  },
};
